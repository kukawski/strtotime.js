module.exports.ts = function ts (arg) {
    var date;

    if (typeof arg === "number") {
        date = new Date(arg);
    } else if (!arg) {
        date = new Date();
    } else {
        date = new Date(arg);
    }

    return Math.floor(date.getTime() / 1000);
};