var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('clf', function () {
    it('should handle days and tz offset with leading zeros', function () {
        assert.strictEqual(strtotime('01/jan/2018:00:01:02 -03:00'), ts('2018-01-01T03:01:02Z'));
        assert.strictEqual(strtotime('02/feb/2019:03:04:05 -02:30'), ts('2019-02-02T05:34:05Z'));
        assert.strictEqual(strtotime('03/mar/2020:06:07:08 -02:00'), ts('2020-03-03T08:07:08Z'));
        assert.strictEqual(strtotime('04/apr/2021:09:10:11 -01:30'), ts('2021-04-04T10:40:11Z'));
        assert.strictEqual(strtotime('05/may/2022:12:13:14 -01:00'), ts('2022-05-05T13:13:14Z'));
        assert.strictEqual(strtotime('06/jun/2023:15:16:17 -00:30'), ts('2023-06-06T15:46:17Z'));
        assert.strictEqual(strtotime('07/jul/2024:18:19:20 -00:00'), ts('2024-07-07T18:19:20Z'));
        assert.strictEqual(strtotime('08/aug/2025:21:22:23 +00:30'), ts('2025-08-08T20:52:23Z'));
        assert.strictEqual(strtotime('09/sep/2026:24:25:26 +01:00'), ts('2026-09-09T23:25:26Z'));
        assert.strictEqual(strtotime('10/sept/2027:00:27:28 +01:30'), ts('2027-09-09T22:57:28Z'));
        assert.strictEqual(strtotime('11/oct/2028:01:29:30 +02:00'), ts('2028-10-10T23:29:30Z'));
        assert.strictEqual(strtotime('12/nov/2029:02:31:32 +02:30'), ts('2029-11-12T00:01:32Z'));
        assert.strictEqual(strtotime('13/dec/2030:03:33:34 +03:00'), ts('2030-12-13T00:33:34Z'));
    });

    it('should handle day and tz offset without leading zeros', function () {
        assert.strictEqual(strtotime('1/jan/2018:00:01:02 GMT-2:0'), ts('2018-01-01T02:01:02Z'));
        assert.strictEqual(strtotime('2/feb/2019:03:04:05 GMT-1:30'), ts('2019-02-02T04:34:05Z'));
        assert.strictEqual(strtotime('3/mar/2020:06:07:08 GMT-1:0'), ts('2020-03-03T07:07:08Z'));
        assert.strictEqual(strtotime('4/apr/2021:09:10:11 GMT-0:30'), ts('2021-04-04T09:40:11Z'));
        assert.strictEqual(strtotime('5/may/2022:12:13:14 GMT+0:0'), ts('2022-05-05T12:13:14Z'));
        assert.strictEqual(strtotime('6/jun/2023:15:16:17 GMT+0:30'), ts('2023-06-06T14:46:17Z'));
        assert.strictEqual(strtotime('7/jul/2024:18:19:20 GMT+1:0'), ts('2024-07-07T17:19:20Z'));
        assert.strictEqual(strtotime('8/aug/2025:21:22:23 GMT+1:30'), ts('2025-08-08T19:52:23Z'));
        assert.strictEqual(strtotime('9/sep/2026:24:25:26 GMT+2:0'), ts('2026-09-09T22:25:26Z'));
    });

    it('it should handle day suffixes', function () {
        assert.strictEqual(strtotime('1st/jan/2018:00:01:02 -03:00'), ts('2018-01-01T03:01:02Z'));
        assert.strictEqual(strtotime('02nd/feb/2019:03:04:05 -02:30'), ts('2019-02-02T05:34:05Z'));
        assert.strictEqual(strtotime('3rd/mar/2020:06:07:08 -02:00'), ts('2020-03-03T08:07:08Z'));
        assert.strictEqual(strtotime('04th/apr/2021:09:10:11 -01:30'), ts('2021-04-04T10:40:11Z'));
    });

    it('should handle 0th day, 24th hour and 60 second', function () {
        assert.strictEqual(strtotime('0th/jan/2017:24:59:60 -9:00'), ts('2017-01-01T10:00:00Z'));
        assert.strictEqual(strtotime('00st/dec/2017:24:59:60 +05:0'), ts('2017-11-30T20:00:00Z'));
    });
});