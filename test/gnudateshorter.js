var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('gnudateshorter', function () {
  it('month without leading zero', function () {
    assert.strictEqual(strtotime('2017-0', now), ts('2016-12-01T00:00:00'));
    assert.strictEqual(strtotime('2017-1', now), ts('2017-01-01T00:00:00'));
    assert.strictEqual(strtotime('2017-2', now), ts('2017-02-01T00:00:00'));
    assert.strictEqual(strtotime('2017-3', now), ts('2017-03-01T00:00:00'));
    assert.strictEqual(strtotime('2017-4', now), ts('2017-04-01T00:00:00'));
    assert.strictEqual(strtotime('2017-5', now), ts('2017-05-01T00:00:00'));
    assert.strictEqual(strtotime('2017-6', now), ts('2017-06-01T00:00:00'));
    assert.strictEqual(strtotime('2017-7', now), ts('2017-07-01T00:00:00'));
    assert.strictEqual(strtotime('2017-8', now), ts('2017-08-01T00:00:00'));
    assert.strictEqual(strtotime('2017-9', now), ts('2017-09-01T00:00:00'));
    assert.strictEqual(strtotime('2017-10', now), ts('2017-10-01T00:00:00'));
    assert.strictEqual(strtotime('2017-11', now), ts('2017-11-01T00:00:00'));
    assert.strictEqual(strtotime('2017-12', now), ts('2017-12-01T00:00:00'));
  });

  it('month with leading zero', function () {
    assert.strictEqual(strtotime('2017-00', now), ts('2016-12-01T00:00:00'));
    assert.strictEqual(strtotime('2017-01', now), ts('2017-01-01T00:00:00'));
    assert.strictEqual(strtotime('2017-02', now), ts('2017-02-01T00:00:00'));
    assert.strictEqual(strtotime('2017-03', now), ts('2017-03-01T00:00:00'));
    assert.strictEqual(strtotime('2017-04', now), ts('2017-04-01T00:00:00'));
    assert.strictEqual(strtotime('2017-05', now), ts('2017-05-01T00:00:00'));
    assert.strictEqual(strtotime('2017-06', now), ts('2017-06-01T00:00:00'));
    assert.strictEqual(strtotime('2017-07', now), ts('2017-07-01T00:00:00'));
    assert.strictEqual(strtotime('2017-08', now), ts('2017-08-01T00:00:00'));
    assert.strictEqual(strtotime('2017-09', now), ts('2017-09-01T00:00:00'));
  });
});