var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('timeshort12', function () {
    it('should return correct timestamp', function () {
        assert.strictEqual(strtotime('1:01am ', now), ts('2017-03-14T01:01:00'));
        assert.strictEqual(strtotime('2:02 am ', now), ts('2017-03-14T02:02:00'));
        assert.strictEqual(strtotime('3:03a.m ', now), ts('2017-03-14T03:03:00'));
        assert.strictEqual(strtotime('4:04a.m. ', now), ts('2017-03-14T04:04:00'));
        assert.strictEqual(strtotime('5:05 am ', now), ts('2017-03-14T05:05:00'));
        assert.strictEqual(strtotime('6:06 a.m ', now), ts('2017-03-14T06:06:00'));
        assert.strictEqual(strtotime('7:07 a.m. ', now), ts('2017-03-14T07:07:00'));
        assert.strictEqual(strtotime('1:08pm ', now), ts('2017-03-14T13:08:00'));
        assert.strictEqual(strtotime('2:09 pm ', now), ts('2017-03-14T14:09:00'));
        assert.strictEqual(strtotime('3:10p.m ', now), ts('2017-03-14T15:10:00'));
        assert.strictEqual(strtotime('4:11p.m. ', now), ts('2017-03-14T16:11:00'));
        assert.strictEqual(strtotime('5:12 pm ', now), ts('2017-03-14T17:12:00'));
        assert.strictEqual(strtotime('6:13 p.m ', now), ts('2017-03-14T18:13:00'));
        assert.strictEqual(strtotime('7:14 p.m. ', now), ts('2017-03-14T19:14:00'));
        assert.strictEqual(strtotime('8.15am ', now), ts('2017-03-14T08:15:00'));
        assert.strictEqual(strtotime('8.16a.m ', now), ts('2017-03-14T08:16:00'));
        assert.strictEqual(strtotime('8.17a.m. ', now), ts('2017-03-14T08:17:00'));
        assert.strictEqual(strtotime('8.18am. ', now), ts('2017-03-14T08:18:00'));
        assert.strictEqual(strtotime('8.19 am ', now), ts('2017-03-14T08:19:00'));
        assert.strictEqual(strtotime('8.20 a.m ', now), ts('2017-03-14T08:20:00'));
        assert.strictEqual(strtotime('8.21 am. ', now), ts('2017-03-14T08:21:00'));
        assert.strictEqual(strtotime('8.22 a.m. ', now), ts('2017-03-14T08:22:00'));
    });
});