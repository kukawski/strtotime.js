var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('relativetextweek', function() {
    it('should return correct timestamp', function() {
        assert.strictEqual(strtotime('this week', now), ts('2017-03-13T12:30:45'));
        assert.strictEqual(strtotime('last week', now), ts('2017-03-06T12:30:45'));
        assert.strictEqual(strtotime('previous week', now), ts('2017-03-06T12:30:45'));
        assert.strictEqual(strtotime('next week', now), ts('2017-03-20T12:30:45'));
    });
});