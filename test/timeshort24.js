var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('timeshort24', function () {
    it('should return correct timestamp', function () {
        assert.strictEqual(strtotime('t00:00', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('t00.00', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('00:00', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('00.00', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('t0:0', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('t0.0', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('0:0', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('0.0', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('00:0', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('0:00', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('00.0', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('0.00', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('24:00', now), ts('2017-03-15T00:00:00'));
        assert.strictEqual(strtotime('24.00', now), ts('2017-03-15T00:00:00'));
        assert.strictEqual(strtotime('t24:00', now), ts('2017-03-15T00:00:00'));
        assert.strictEqual(strtotime('t24.00', now), ts('2017-03-15T00:00:00'));
    });
});