var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('relativetext', function() {
    it('seconds', function() {
        assert.strictEqual(strtotime('first sec', now), ts('2017-03-14T12:30:46'));
        assert.strictEqual(strtotime('first secs', now), ts('2017-03-14T12:30:46'));
        assert.strictEqual(strtotime('first second', now), ts('2017-03-14T12:30:46'));
        assert.strictEqual(strtotime('first seconds', now), ts('2017-03-14T12:30:46'));

        assert.strictEqual(strtotime('second sec', now), ts('2017-03-14T12:30:47'));
        assert.strictEqual(strtotime('second secs', now), ts('2017-03-14T12:30:47'));
        assert.strictEqual(strtotime('second second', now), ts('2017-03-14T12:30:47'));
        assert.strictEqual(strtotime('second seconds', now), ts('2017-03-14T12:30:47'));

        assert.strictEqual(strtotime('third sec', now), ts('2017-03-14T12:30:48'));
        assert.strictEqual(strtotime('third secs', now), ts('2017-03-14T12:30:48'));
        assert.strictEqual(strtotime('third second', now), ts('2017-03-14T12:30:48'));
        assert.strictEqual(strtotime('third seconds', now), ts('2017-03-14T12:30:48'));

        assert.strictEqual(strtotime('fourth sec', now), ts('2017-03-14T12:30:49'));
        assert.strictEqual(strtotime('fourth secs', now), ts('2017-03-14T12:30:49'));
        assert.strictEqual(strtotime('fourth second', now), ts('2017-03-14T12:30:49'));
        assert.strictEqual(strtotime('fourth seconds', now), ts('2017-03-14T12:30:49'));

        assert.strictEqual(strtotime('fifth sec', now), ts('2017-03-14T12:30:50'));
        assert.strictEqual(strtotime('fifth secs', now), ts('2017-03-14T12:30:50'));
        assert.strictEqual(strtotime('fifth second', now), ts('2017-03-14T12:30:50'));
        assert.strictEqual(strtotime('fifth seconds', now), ts('2017-03-14T12:30:50'));

        assert.strictEqual(strtotime('sixth sec', now), ts('2017-03-14T12:30:51'));
        assert.strictEqual(strtotime('sixth secs', now), ts('2017-03-14T12:30:51'));
        assert.strictEqual(strtotime('sixth second', now), ts('2017-03-14T12:30:51'));
        assert.strictEqual(strtotime('sixth seconds', now), ts('2017-03-14T12:30:51'));

        assert.strictEqual(strtotime('seventh sec', now), ts('2017-03-14T12:30:52'));
        assert.strictEqual(strtotime('seventh secs', now), ts('2017-03-14T12:30:52'));
        assert.strictEqual(strtotime('seventh second', now), ts('2017-03-14T12:30:52'));
        assert.strictEqual(strtotime('seventh seconds', now), ts('2017-03-14T12:30:52'));

        assert.strictEqual(strtotime('eight sec', now), ts('2017-03-14T12:30:53'));
        assert.strictEqual(strtotime('eight secs', now), ts('2017-03-14T12:30:53'));
        assert.strictEqual(strtotime('eight second', now), ts('2017-03-14T12:30:53'));
        assert.strictEqual(strtotime('eight seconds', now), ts('2017-03-14T12:30:53'));

        assert.strictEqual(strtotime('eighth sec', now), ts('2017-03-14T12:30:53'));
        assert.strictEqual(strtotime('eighth secs', now), ts('2017-03-14T12:30:53'));
        assert.strictEqual(strtotime('eighth second', now), ts('2017-03-14T12:30:53'));
        assert.strictEqual(strtotime('eighth seconds', now), ts('2017-03-14T12:30:53'));

        assert.strictEqual(strtotime('ninth sec', now), ts('2017-03-14T12:30:54'));
        assert.strictEqual(strtotime('ninth secs', now), ts('2017-03-14T12:30:54'));
        assert.strictEqual(strtotime('ninth second', now), ts('2017-03-14T12:30:54'));
        assert.strictEqual(strtotime('ninth seconds', now), ts('2017-03-14T12:30:54'));

        assert.strictEqual(strtotime('tenth sec', now), ts('2017-03-14T12:30:55'));
        assert.strictEqual(strtotime('tenth secs', now), ts('2017-03-14T12:30:55'));
        assert.strictEqual(strtotime('tenth second', now), ts('2017-03-14T12:30:55'));
        assert.strictEqual(strtotime('tenth seconds', now), ts('2017-03-14T12:30:55'));

        assert.strictEqual(strtotime('eleventh sec', now), ts('2017-03-14T12:30:56'));
        assert.strictEqual(strtotime('eleventh secs', now), ts('2017-03-14T12:30:56'));
        assert.strictEqual(strtotime('eleventh second', now), ts('2017-03-14T12:30:56'));
        assert.strictEqual(strtotime('eleventh seconds', now), ts('2017-03-14T12:30:56'));

        assert.strictEqual(strtotime('twelfth sec', now), ts('2017-03-14T12:30:57'));
        assert.strictEqual(strtotime('twelfth secs', now), ts('2017-03-14T12:30:57'));
        assert.strictEqual(strtotime('twelfth second', now), ts('2017-03-14T12:30:57'));
        assert.strictEqual(strtotime('twelfth seconds', now), ts('2017-03-14T12:30:57'));

        assert.strictEqual(strtotime('this sec', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this secs', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this second', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this seconds', now), ts('2017-03-14T12:30:45'));

        assert.strictEqual(strtotime('last sec', now), ts('2017-03-14T12:30:44'));
        assert.strictEqual(strtotime('last secs', now), ts('2017-03-14T12:30:44'));
        assert.strictEqual(strtotime('last second', now), ts('2017-03-14T12:30:44'));
        assert.strictEqual(strtotime('last seconds', now), ts('2017-03-14T12:30:44'));

        assert.strictEqual(strtotime('previous sec', now), ts('2017-03-14T12:30:44'));
        assert.strictEqual(strtotime('previous secs', now), ts('2017-03-14T12:30:44'));
        assert.strictEqual(strtotime('previous second', now), ts('2017-03-14T12:30:44'));
        assert.strictEqual(strtotime('previous seconds', now), ts('2017-03-14T12:30:44'));

        assert.strictEqual(strtotime('next sec', now), ts('2017-03-14T12:30:46'));
        assert.strictEqual(strtotime('next secs', now), ts('2017-03-14T12:30:46'));
        assert.strictEqual(strtotime('next second', now), ts('2017-03-14T12:30:46'));
        assert.strictEqual(strtotime('next seconds', now), ts('2017-03-14T12:30:46'));
    });

    it('minutes', function() {
        assert.strictEqual(strtotime('first min', now), ts('2017-03-14T12:31:45'));
        assert.strictEqual(strtotime('first mins', now), ts('2017-03-14T12:31:45'));
        assert.strictEqual(strtotime('first minute', now), ts('2017-03-14T12:31:45'));
        assert.strictEqual(strtotime('first minutes', now), ts('2017-03-14T12:31:45'));

        assert.strictEqual(strtotime('second min', now), ts('2017-03-14T12:32:45'));
        assert.strictEqual(strtotime('second mins', now), ts('2017-03-14T12:32:45'));
        assert.strictEqual(strtotime('second minute', now), ts('2017-03-14T12:32:45'));
        assert.strictEqual(strtotime('second minutes', now), ts('2017-03-14T12:32:45'));

        assert.strictEqual(strtotime('third min', now), ts('2017-03-14T12:33:45'));
        assert.strictEqual(strtotime('third mins', now), ts('2017-03-14T12:33:45'));
        assert.strictEqual(strtotime('third minute', now), ts('2017-03-14T12:33:45'));
        assert.strictEqual(strtotime('third minutes', now), ts('2017-03-14T12:33:45'));

        assert.strictEqual(strtotime('fourth min', now), ts('2017-03-14T12:34:45'));
        assert.strictEqual(strtotime('fourth mins', now), ts('2017-03-14T12:34:45'));
        assert.strictEqual(strtotime('fourth minute', now), ts('2017-03-14T12:34:45'));
        assert.strictEqual(strtotime('fourth minutes', now), ts('2017-03-14T12:34:45'));

        assert.strictEqual(strtotime('fifth min', now), ts('2017-03-14T12:35:45'));
        assert.strictEqual(strtotime('fifth mins', now), ts('2017-03-14T12:35:45'));
        assert.strictEqual(strtotime('fifth minute', now), ts('2017-03-14T12:35:45'));
        assert.strictEqual(strtotime('fifth minutes', now), ts('2017-03-14T12:35:45'));

        assert.strictEqual(strtotime('sixth min', now), ts('2017-03-14T12:36:45'));
        assert.strictEqual(strtotime('sixth mins', now), ts('2017-03-14T12:36:45'));
        assert.strictEqual(strtotime('sixth minute', now), ts('2017-03-14T12:36:45'));
        assert.strictEqual(strtotime('sixth minutes', now), ts('2017-03-14T12:36:45'));

        assert.strictEqual(strtotime('seventh min', now), ts('2017-03-14T12:37:45'));
        assert.strictEqual(strtotime('seventh mins', now), ts('2017-03-14T12:37:45'));
        assert.strictEqual(strtotime('seventh minute', now), ts('2017-03-14T12:37:45'));
        assert.strictEqual(strtotime('seventh minutes', now), ts('2017-03-14T12:37:45'));

        assert.strictEqual(strtotime('eight min', now), ts('2017-03-14T12:38:45'));
        assert.strictEqual(strtotime('eight mins', now), ts('2017-03-14T12:38:45'));
        assert.strictEqual(strtotime('eight minute', now), ts('2017-03-14T12:38:45'));
        assert.strictEqual(strtotime('eight minutes', now), ts('2017-03-14T12:38:45'));

        assert.strictEqual(strtotime('eighth min', now), ts('2017-03-14T12:38:45'));
        assert.strictEqual(strtotime('eighth mins', now), ts('2017-03-14T12:38:45'));
        assert.strictEqual(strtotime('eighth minute', now), ts('2017-03-14T12:38:45'));
        assert.strictEqual(strtotime('eighth minutes', now), ts('2017-03-14T12:38:45'));

        assert.strictEqual(strtotime('ninth min', now), ts('2017-03-14T12:39:45'));
        assert.strictEqual(strtotime('ninth mins', now), ts('2017-03-14T12:39:45'));
        assert.strictEqual(strtotime('ninth minute', now), ts('2017-03-14T12:39:45'));
        assert.strictEqual(strtotime('ninth minutes', now), ts('2017-03-14T12:39:45'));

        assert.strictEqual(strtotime('tenth min', now), ts('2017-03-14T12:40:45'));
        assert.strictEqual(strtotime('tenth mins', now), ts('2017-03-14T12:40:45'));
        assert.strictEqual(strtotime('tenth minute', now), ts('2017-03-14T12:40:45'));
        assert.strictEqual(strtotime('tenth minutes', now), ts('2017-03-14T12:40:45'));

        assert.strictEqual(strtotime('eleventh min', now), ts('2017-03-14T12:41:45'));
        assert.strictEqual(strtotime('eleventh mins', now), ts('2017-03-14T12:41:45'));
        assert.strictEqual(strtotime('eleventh minute', now), ts('2017-03-14T12:41:45'));
        assert.strictEqual(strtotime('eleventh minutes', now), ts('2017-03-14T12:41:45'));

        assert.strictEqual(strtotime('twelfth min', now), ts('2017-03-14T12:42:45'));
        assert.strictEqual(strtotime('twelfth mins', now), ts('2017-03-14T12:42:45'));
        assert.strictEqual(strtotime('twelfth minute', now), ts('2017-03-14T12:42:45'));
        assert.strictEqual(strtotime('twelfth minutes', now), ts('2017-03-14T12:42:45'));

        assert.strictEqual(strtotime('this min', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this mins', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this minute', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this minutes', now), ts('2017-03-14T12:30:45'));

        assert.strictEqual(strtotime('last min', now), ts('2017-03-14T12:29:45'));
        assert.strictEqual(strtotime('last mins', now), ts('2017-03-14T12:29:45'));
        assert.strictEqual(strtotime('last minute', now), ts('2017-03-14T12:29:45'));
        assert.strictEqual(strtotime('last minutes', now), ts('2017-03-14T12:29:45'));

        assert.strictEqual(strtotime('previous min', now), ts('2017-03-14T12:29:45'));
        assert.strictEqual(strtotime('previous mins', now), ts('2017-03-14T12:29:45'));
        assert.strictEqual(strtotime('previous minute', now), ts('2017-03-14T12:29:45'));
        assert.strictEqual(strtotime('previous minutes', now), ts('2017-03-14T12:29:45'));

        assert.strictEqual(strtotime('next min', now), ts('2017-03-14T12:31:45'));
        assert.strictEqual(strtotime('next mins', now), ts('2017-03-14T12:31:45'));
        assert.strictEqual(strtotime('next minute', now), ts('2017-03-14T12:31:45'));
        assert.strictEqual(strtotime('next minutes', now), ts('2017-03-14T12:31:45'));
    });

    it('hours', function() {
        assert.strictEqual(strtotime('first hour', now), ts('2017-03-14T13:30:45'));
        assert.strictEqual(strtotime('first hours', now), ts('2017-03-14T13:30:45'));

        assert.strictEqual(strtotime('second hour', now), ts('2017-03-14T14:30:45'));
        assert.strictEqual(strtotime('second hours', now), ts('2017-03-14T14:30:45'));

        assert.strictEqual(strtotime('third hour', now), ts('2017-03-14T15:30:45'));
        assert.strictEqual(strtotime('third hours', now), ts('2017-03-14T15:30:45'));

        assert.strictEqual(strtotime('fourth hour', now), ts('2017-03-14T16:30:45'));
        assert.strictEqual(strtotime('fourth hours', now), ts('2017-03-14T16:30:45'));

        assert.strictEqual(strtotime('fifth hour', now), ts('2017-03-14T17:30:45'));
        assert.strictEqual(strtotime('fifth hours', now), ts('2017-03-14T17:30:45'));

        assert.strictEqual(strtotime('sixth hour', now), ts('2017-03-14T18:30:45'));
        assert.strictEqual(strtotime('sixth hours', now), ts('2017-03-14T18:30:45'));

        assert.strictEqual(strtotime('seventh hour', now), ts('2017-03-14T19:30:45'));
        assert.strictEqual(strtotime('seventh hours', now), ts('2017-03-14T19:30:45'));

        assert.strictEqual(strtotime('eight hour', now), ts('2017-03-14T20:30:45'));
        assert.strictEqual(strtotime('eight hours', now), ts('2017-03-14T20:30:45'));

        assert.strictEqual(strtotime('eighth hour', now), ts('2017-03-14T20:30:45'));
        assert.strictEqual(strtotime('eighth hours', now), ts('2017-03-14T20:30:45'));

        assert.strictEqual(strtotime('ninth hour', now), ts('2017-03-14T21:30:45'));
        assert.strictEqual(strtotime('ninth hours', now), ts('2017-03-14T21:30:45'));

        assert.strictEqual(strtotime('tenth hour', now), ts('2017-03-14T22:30:45'));
        assert.strictEqual(strtotime('tenth hours', now), ts('2017-03-14T22:30:45'));

        assert.strictEqual(strtotime('eleventh hour', now), ts('2017-03-14T23:30:45'));
        assert.strictEqual(strtotime('eleventh hours', now), ts('2017-03-14T23:30:45'));

        assert.strictEqual(strtotime('twelfth hour', now), ts('2017-03-15T00:30:45'));
        assert.strictEqual(strtotime('twelfth hours', now), ts('2017-03-15T00:30:45'));

        assert.strictEqual(strtotime('this hour', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this hours', now), ts('2017-03-14T12:30:45'));

        assert.strictEqual(strtotime('last hour', now), ts('2017-03-14T11:30:45'));
        assert.strictEqual(strtotime('last hours', now), ts('2017-03-14T11:30:45'));

        assert.strictEqual(strtotime('previous hour', now), ts('2017-03-14T11:30:45'));
        assert.strictEqual(strtotime('previous hours', now), ts('2017-03-14T11:30:45'));

        assert.strictEqual(strtotime('next hour', now), ts('2017-03-14T13:30:45'));
        assert.strictEqual(strtotime('next hours', now), ts('2017-03-14T13:30:45'));
    });

    it('days', function() {
        assert.strictEqual(strtotime('first day', now), ts('2017-03-15T12:30:45'));
        assert.strictEqual(strtotime('first days', now), ts('2017-03-15T12:30:45'));

        assert.strictEqual(strtotime('second day', now), ts('2017-03-16T12:30:45'));
        assert.strictEqual(strtotime('second days', now), ts('2017-03-16T12:30:45'));

        assert.strictEqual(strtotime('third day', now), ts('2017-03-17T12:30:45'));
        assert.strictEqual(strtotime('third days', now), ts('2017-03-17T12:30:45'));

        assert.strictEqual(strtotime('fourth day', now), ts('2017-03-18T12:30:45'));
        assert.strictEqual(strtotime('fourth days', now), ts('2017-03-18T12:30:45'));

        assert.strictEqual(strtotime('fifth day', now), ts('2017-03-19T12:30:45'));
        assert.strictEqual(strtotime('fifth days', now), ts('2017-03-19T12:30:45'));

        assert.strictEqual(strtotime('sixth day', now), ts('2017-03-20T12:30:45'));
        assert.strictEqual(strtotime('sixth days', now), ts('2017-03-20T12:30:45'));

        assert.strictEqual(strtotime('seventh day', now), ts('2017-03-21T12:30:45'));
        assert.strictEqual(strtotime('seventh days', now), ts('2017-03-21T12:30:45'));

        assert.strictEqual(strtotime('eight day', now), ts('2017-03-22T12:30:45'));
        assert.strictEqual(strtotime('eight days', now), ts('2017-03-22T12:30:45'));

        assert.strictEqual(strtotime('eighth day', now), ts('2017-03-22T12:30:45'));
        assert.strictEqual(strtotime('eighth days', now), ts('2017-03-22T12:30:45'));

        assert.strictEqual(strtotime('ninth day', now), ts('2017-03-23T12:30:45'));
        assert.strictEqual(strtotime('ninth days', now), ts('2017-03-23T12:30:45'));

        assert.strictEqual(strtotime('tenth day', now), ts('2017-03-24T12:30:45'));
        assert.strictEqual(strtotime('tenth days', now), ts('2017-03-24T12:30:45'));

        assert.strictEqual(strtotime('eleventh day', now), ts('2017-03-25T12:30:45'));
        assert.strictEqual(strtotime('eleventh days', now), ts('2017-03-25T12:30:45'));

        assert.strictEqual(strtotime('twelfth day', now), ts('2017-03-26T12:30:45'));
        assert.strictEqual(strtotime('twelfth days', now), ts('2017-03-26T12:30:45'));

        assert.strictEqual(strtotime('this day', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this days', now), ts('2017-03-14T12:30:45'));

        assert.strictEqual(strtotime('last day', now), ts('2017-03-13T12:30:45'));
        assert.strictEqual(strtotime('last days', now), ts('2017-03-13T12:30:45'));

        assert.strictEqual(strtotime('previous day', now), ts('2017-03-13T12:30:45'));
        assert.strictEqual(strtotime('previous days', now), ts('2017-03-13T12:30:45'));

        assert.strictEqual(strtotime('next day', now), ts('2017-03-15T12:30:45'));
        assert.strictEqual(strtotime('next days', now), ts('2017-03-15T12:30:45'));
    });

    it('fortnights', function() {
        assert.strictEqual(strtotime('first fortnight', now), ts('2017-03-28T12:30:45'));
        assert.strictEqual(strtotime('first fortnights', now), ts('2017-03-28T12:30:45'));
        assert.strictEqual(strtotime('first forthnight', now), ts('2017-03-28T12:30:45'));
        assert.strictEqual(strtotime('first forthnights', now), ts('2017-03-28T12:30:45'));

        assert.strictEqual(strtotime('second fortnight', now), ts('2017-04-11T12:30:45'));
        assert.strictEqual(strtotime('second fortnights', now), ts('2017-04-11T12:30:45'));
        assert.strictEqual(strtotime('second forthnight', now), ts('2017-04-11T12:30:45'));
        assert.strictEqual(strtotime('second forthnights', now), ts('2017-04-11T12:30:45'));

        assert.strictEqual(strtotime('third fortnight', now), ts('2017-04-25T12:30:45'));
        assert.strictEqual(strtotime('third fortnights', now), ts('2017-04-25T12:30:45'));
        assert.strictEqual(strtotime('third forthnight', now), ts('2017-04-25T12:30:45'));
        assert.strictEqual(strtotime('third forthnights', now), ts('2017-04-25T12:30:45'));

        assert.strictEqual(strtotime('fourth fortnight', now), ts('2017-05-09T12:30:45'));
        assert.strictEqual(strtotime('fourth fortnights', now), ts('2017-05-09T12:30:45'));
        assert.strictEqual(strtotime('fourth forthnight', now), ts('2017-05-09T12:30:45'));
        assert.strictEqual(strtotime('fourth forthnights', now), ts('2017-05-09T12:30:45'));

        assert.strictEqual(strtotime('fifth fortnight', now), ts('2017-05-23T12:30:45'));
        assert.strictEqual(strtotime('fifth fortnights', now), ts('2017-05-23T12:30:45'));
        assert.strictEqual(strtotime('fifth forthnight', now), ts('2017-05-23T12:30:45'));
        assert.strictEqual(strtotime('fifth forthnights', now), ts('2017-05-23T12:30:45'));

        assert.strictEqual(strtotime('sixth fortnight', now), ts('2017-06-06T12:30:45'));
        assert.strictEqual(strtotime('sixth fortnights', now), ts('2017-06-06T12:30:45'));
        assert.strictEqual(strtotime('sixth forthnight', now), ts('2017-06-06T12:30:45'));
        assert.strictEqual(strtotime('sixth forthnights', now), ts('2017-06-06T12:30:45'));

        assert.strictEqual(strtotime('seventh fortnight', now), ts('2017-06-20T12:30:45'));
        assert.strictEqual(strtotime('seventh fortnights', now), ts('2017-06-20T12:30:45'));
        assert.strictEqual(strtotime('seventh forthnight', now), ts('2017-06-20T12:30:45'));
        assert.strictEqual(strtotime('seventh forthnights', now), ts('2017-06-20T12:30:45'));

        assert.strictEqual(strtotime('eight fortnight', now), ts('2017-07-04T12:30:45'));
        assert.strictEqual(strtotime('eight fortnights', now), ts('2017-07-04T12:30:45'));
        assert.strictEqual(strtotime('eight forthnight', now), ts('2017-07-04T12:30:45'));
        assert.strictEqual(strtotime('eight forthnights', now), ts('2017-07-04T12:30:45'));

        assert.strictEqual(strtotime('eighth fortnight', now), ts('2017-07-04T12:30:45'));
        assert.strictEqual(strtotime('eighth fortnights', now), ts('2017-07-04T12:30:45'));
        assert.strictEqual(strtotime('eighth forthnight', now), ts('2017-07-04T12:30:45'));
        assert.strictEqual(strtotime('eighth forthnights', now), ts('2017-07-04T12:30:45'));

        assert.strictEqual(strtotime('ninth fortnight', now), ts('2017-07-18T12:30:45'));
        assert.strictEqual(strtotime('ninth fortnights', now), ts('2017-07-18T12:30:45'));
        assert.strictEqual(strtotime('ninth forthnight', now), ts('2017-07-18T12:30:45'));
        assert.strictEqual(strtotime('ninth forthnights', now), ts('2017-07-18T12:30:45'));

        assert.strictEqual(strtotime('tenth fortnight', now), ts('2017-08-01T12:30:45'));
        assert.strictEqual(strtotime('tenth fortnights', now), ts('2017-08-01T12:30:45'));
        assert.strictEqual(strtotime('tenth forthnight', now), ts('2017-08-01T12:30:45'));
        assert.strictEqual(strtotime('tenth forthnights', now), ts('2017-08-01T12:30:45'));

        assert.strictEqual(strtotime('eleventh fortnight', now), ts('2017-08-15T12:30:45'));
        assert.strictEqual(strtotime('eleventh fortnights', now), ts('2017-08-15T12:30:45'));
        assert.strictEqual(strtotime('eleventh forthnight', now), ts('2017-08-15T12:30:45'));
        assert.strictEqual(strtotime('eleventh forthnights', now), ts('2017-08-15T12:30:45'));

        assert.strictEqual(strtotime('twelfth fortnight', now), ts('2017-08-29T12:30:45'));
        assert.strictEqual(strtotime('twelfth fortnights', now), ts('2017-08-29T12:30:45'));
        assert.strictEqual(strtotime('twelfth forthnight', now), ts('2017-08-29T12:30:45'));
        assert.strictEqual(strtotime('twelfth forthnights', now), ts('2017-08-29T12:30:45'));

        assert.strictEqual(strtotime('this fortnight', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this fortnights', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this forthnight', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this forthnights', now), ts('2017-03-14T12:30:45'));

        assert.strictEqual(strtotime('last fortnight', now), ts('2017-02-28T12:30:45'));
        assert.strictEqual(strtotime('last fortnights', now), ts('2017-02-28T12:30:45'));
        assert.strictEqual(strtotime('last forthnight', now), ts('2017-02-28T12:30:45'));
        assert.strictEqual(strtotime('last forthnights', now), ts('2017-02-28T12:30:45'));

        assert.strictEqual(strtotime('previous fortnight', now), ts('2017-02-28T12:30:45'));
        assert.strictEqual(strtotime('previous fortnights', now), ts('2017-02-28T12:30:45'));
        assert.strictEqual(strtotime('previous forthnight', now), ts('2017-02-28T12:30:45'));
        assert.strictEqual(strtotime('previous forthnights', now), ts('2017-02-28T12:30:45'));

        assert.strictEqual(strtotime('next fortnight', now), ts('2017-03-28T12:30:45'));
        assert.strictEqual(strtotime('next fortnights', now), ts('2017-03-28T12:30:45'));
        assert.strictEqual(strtotime('next forthnight', now), ts('2017-03-28T12:30:45'));
        assert.strictEqual(strtotime('next forthnights', now), ts('2017-03-28T12:30:45'));
    });

    it('weeks', function() {
        assert.strictEqual(strtotime('first weeks', now), ts('2017-03-21T12:30:45'));
        assert.strictEqual(strtotime('second weeks', now), ts('2017-03-28T12:30:45'));
        assert.strictEqual(strtotime('third weeks', now), ts('2017-04-04T12:30:45'));
        assert.strictEqual(strtotime('fourth weeks', now), ts('2017-04-11T12:30:45'));
        assert.strictEqual(strtotime('fifth weeks', now), ts('2017-04-18T12:30:45'));
        assert.strictEqual(strtotime('sixth weeks', now), ts('2017-04-25T12:30:45'));
        assert.strictEqual(strtotime('seventh weeks', now), ts('2017-05-02T12:30:45'));
        assert.strictEqual(strtotime('eight weeks', now), ts('2017-05-09T12:30:45'));
        assert.strictEqual(strtotime('eighth weeks', now), ts('2017-05-09T12:30:45'));
        assert.strictEqual(strtotime('ninth weeks', now), ts('2017-05-16T12:30:45'));
        assert.strictEqual(strtotime('tenth weeks', now), ts('2017-05-23T12:30:45'));
        assert.strictEqual(strtotime('eleventh weeks', now), ts('2017-05-30T12:30:45'));
        assert.strictEqual(strtotime('twelfth weeks', now), ts('2017-06-06T12:30:45'));
        assert.strictEqual(strtotime('this weeks', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('last weeks', now), ts('2017-03-07T12:30:45'));
        assert.strictEqual(strtotime('previous weeks', now), ts('2017-03-07T12:30:45'));
        assert.strictEqual(strtotime('next weeks', now), ts('2017-03-21T12:30:45'));
    });

    it('months', function() {
        assert.strictEqual(strtotime('first month', now), ts('2017-04-14T12:30:45'));
        assert.strictEqual(strtotime('first months', now), ts('2017-04-14T12:30:45'));

        assert.strictEqual(strtotime('second month', now), ts('2017-05-14T12:30:45'));
        assert.strictEqual(strtotime('second months', now), ts('2017-05-14T12:30:45'));

        assert.strictEqual(strtotime('third month', now), ts('2017-06-14T12:30:45'));
        assert.strictEqual(strtotime('third months', now), ts('2017-06-14T12:30:45'));

        assert.strictEqual(strtotime('fourth month', now), ts('2017-07-14T12:30:45'));
        assert.strictEqual(strtotime('fourth months', now), ts('2017-07-14T12:30:45'));

        assert.strictEqual(strtotime('fifth month', now), ts('2017-08-14T12:30:45'));
        assert.strictEqual(strtotime('fifth months', now), ts('2017-08-14T12:30:45'));

        assert.strictEqual(strtotime('sixth month', now), ts('2017-09-14T12:30:45'));
        assert.strictEqual(strtotime('sixth months', now), ts('2017-09-14T12:30:45'));

        assert.strictEqual(strtotime('seventh month', now), ts('2017-10-14T12:30:45'));
        assert.strictEqual(strtotime('seventh months', now), ts('2017-10-14T12:30:45'));

        assert.strictEqual(strtotime('eight month', now), ts('2017-11-14T12:30:45'));
        assert.strictEqual(strtotime('eight months', now), ts('2017-11-14T12:30:45'));

        assert.strictEqual(strtotime('eighth month', now), ts('2017-11-14T12:30:45'));
        assert.strictEqual(strtotime('eighth months', now), ts('2017-11-14T12:30:45'));

        assert.strictEqual(strtotime('ninth month', now), ts('2017-12-14T12:30:45'));
        assert.strictEqual(strtotime('ninth months', now), ts('2017-12-14T12:30:45'));

        assert.strictEqual(strtotime('tenth month', now), ts('2018-01-14T12:30:45'));
        assert.strictEqual(strtotime('tenth months', now), ts('2018-01-14T12:30:45'));

        assert.strictEqual(strtotime('eleventh month', now), ts('2018-02-14T12:30:45'));
        assert.strictEqual(strtotime('eleventh months', now), ts('2018-02-14T12:30:45'));

        assert.strictEqual(strtotime('twelfth month', now), ts('2018-03-14T12:30:45'));
        assert.strictEqual(strtotime('twelfth months', now), ts('2018-03-14T12:30:45'));

        assert.strictEqual(strtotime('this month', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this months', now), ts('2017-03-14T12:30:45'));

        assert.strictEqual(strtotime('last month', now), ts('2017-02-14T12:30:45'));
        assert.strictEqual(strtotime('last months', now), ts('2017-02-14T12:30:45'));

        assert.strictEqual(strtotime('previous month', now), ts('2017-02-14T12:30:45'));
        assert.strictEqual(strtotime('previous months', now), ts('2017-02-14T12:30:45'));

        assert.strictEqual(strtotime('next month', now), ts('2017-04-14T12:30:45'));
        assert.strictEqual(strtotime('next months', now), ts('2017-04-14T12:30:45'));
    });

    it('years', function() {
        assert.strictEqual(strtotime('first year', now), ts('2018-03-14T12:30:45'));
        assert.strictEqual(strtotime('first years', now), ts('2018-03-14T12:30:45'));

        assert.strictEqual(strtotime('second year', now), ts('2019-03-14T12:30:45'));
        assert.strictEqual(strtotime('second years', now), ts('2019-03-14T12:30:45'));

        assert.strictEqual(strtotime('third year', now), ts('2020-03-14T12:30:45'));
        assert.strictEqual(strtotime('third years', now), ts('2020-03-14T12:30:45'));

        assert.strictEqual(strtotime('fourth year', now), ts('2021-03-14T12:30:45'));
        assert.strictEqual(strtotime('fourth years', now), ts('2021-03-14T12:30:45'));

        assert.strictEqual(strtotime('fifth year', now), ts('2022-03-14T12:30:45'));
        assert.strictEqual(strtotime('fifth years', now), ts('2022-03-14T12:30:45'));

        assert.strictEqual(strtotime('sixth year', now), ts('2023-03-14T12:30:45'));
        assert.strictEqual(strtotime('sixth years', now), ts('2023-03-14T12:30:45'));

        assert.strictEqual(strtotime('seventh year', now), ts('2024-03-14T12:30:45'));
        assert.strictEqual(strtotime('seventh years', now), ts('2024-03-14T12:30:45'));

        assert.strictEqual(strtotime('eight year', now), ts('2025-03-14T12:30:45'));
        assert.strictEqual(strtotime('eight years', now), ts('2025-03-14T12:30:45'));

        assert.strictEqual(strtotime('eighth year', now), ts('2025-03-14T12:30:45'));
        assert.strictEqual(strtotime('eighth years', now), ts('2025-03-14T12:30:45'));

        assert.strictEqual(strtotime('ninth year', now), ts('2026-03-14T12:30:45'));
        assert.strictEqual(strtotime('ninth years', now), ts('2026-03-14T12:30:45'));

        assert.strictEqual(strtotime('tenth year', now), ts('2027-03-14T12:30:45'));
        assert.strictEqual(strtotime('tenth years', now), ts('2027-03-14T12:30:45'));

        assert.strictEqual(strtotime('eleventh year', now), ts('2028-03-14T12:30:45'));
        assert.strictEqual(strtotime('eleventh years', now), ts('2028-03-14T12:30:45'));

        assert.strictEqual(strtotime('twelfth year', now), ts('2029-03-14T12:30:45'));
        assert.strictEqual(strtotime('twelfth years', now), ts('2029-03-14T12:30:45'));

        assert.strictEqual(strtotime('this year', now), ts('2017-03-14T12:30:45'));
        assert.strictEqual(strtotime('this years', now), ts('2017-03-14T12:30:45'));

        assert.strictEqual(strtotime('last year', now), ts('2016-03-14T12:30:45'));
        assert.strictEqual(strtotime('last years', now), ts('2016-03-14T12:30:45'));

        assert.strictEqual(strtotime('previous year', now), ts('2016-03-14T12:30:45'));
        assert.strictEqual(strtotime('previous years', now), ts('2016-03-14T12:30:45'));

        assert.strictEqual(strtotime('next year', now), ts('2018-03-14T12:30:45'));
        assert.strictEqual(strtotime('next years', now), ts('2018-03-14T12:30:45'));
    });
});