var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('xmlrpcnocolon', function () {
    it('should return correct timestamp', function () {
        assert.strictEqual(strtotime('20170913t000000', now), ts('2017-09-13T00:00:00'));
    });

    it('should handle 0th month and 0th day', function () {
        assert.strictEqual(strtotime('20170013t000000', now), ts('2016-12-13T00:00:00'));
        assert.strictEqual(strtotime('20170900t000000', now), ts('2017-08-31T00:00:00'));
        assert.strictEqual(strtotime('20170000t000000', now), ts('2016-11-30T00:00:00'));
    });

    it('should handle day out of bounds', function () {
        assert.strictEqual(strtotime('20170229t000000', now), ts('2017-03-01T00:00:00'));
        assert.strictEqual(strtotime('20160230t000000', now), ts('2016-03-01T00:00:00'));
        assert.strictEqual(strtotime('20170431t000000', now), ts('2017-05-01T00:00:00'));
    });

    it('should handle 24th hour and 60th sec', function () {
        assert.strictEqual(strtotime('20170913t240000', now), ts('2017-09-14T00:00:00'));
        assert.strictEqual(strtotime('20170913t000060', now), ts('2017-09-13T00:01:00'));
        assert.strictEqual(strtotime('20170913t240060', now), ts('2017-09-14T00:01:00'));
        assert.strictEqual(strtotime('20171231t245960', now), ts('2018-01-01T01:00:00'));
    });
});