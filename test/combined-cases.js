var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('combined cases', function () {
  it('two dates are not allowed together', function () {
    assert.strictEqual(strtotime('2017-01-01 2017-02-02'), false);
  });

  it('two times are not allowed together', function () {
    assert.strictEqual(strtotime('09:10:11 12:13'), false);
  });
});