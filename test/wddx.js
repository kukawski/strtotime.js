var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('wddx', function () {
    it('should return correct timestamp', function () {
        assert.strictEqual(strtotime('2017-9-13T5:6:7', now), ts('2017-09-13T05:06:07'));
        assert.strictEqual(strtotime('2017-09-13T05:06:07', now), ts('2017-09-13T05:06:07'));
    });

    it('should handle 0th month, 0th day', function () {
        assert.strictEqual(strtotime('2017-0-13T5:6:7', now), ts('2016-12-13T05:06:07'));
        assert.strictEqual(strtotime('2017-00-13T05:06:07', now), ts('2016-12-13T05:06:07'));

        assert.strictEqual(strtotime('2017-9-0T5:6:7', now), ts('2017-08-31T05:06:07'));
        assert.strictEqual(strtotime('2017-9-00T5:6:7', now), ts('2017-08-31T05:06:07'));
        assert.strictEqual(strtotime('2017-09-0T05:06:07', now), ts('2017-08-31T05:06:07'));
        assert.strictEqual(strtotime('2017-09-00T05:06:07', now), ts('2017-08-31T05:06:07'));
    });

    it('should handle 60th second and 24th hour', function () {
        assert.strictEqual(strtotime('2017-09-13T5:6:60', now), ts('2017-09-13T05:07:00'));
        assert.strictEqual(strtotime('2017-09-13T24:06:07', now), ts('2017-09-14T00:06:07'));
        assert.strictEqual(strtotime('2017-09-13T24:06:60', now), ts('2017-09-14T00:07:00'));
        assert.strictEqual(strtotime('2017-00-00T24:59:60', now), ts('2016-12-01T01:00:00'));
        assert.strictEqual(strtotime('2017-12-31T24:59:60', now), ts('2018-01-01T01:00:00'));
    });

    it('should handle dates out of bounds', function () {
        assert.strictEqual(strtotime('2017-02-29T00:0:00', now), ts('2017-03-01T00:00:00'));
        assert.strictEqual(strtotime('2017-04-31T0:00:0', now), ts('2017-05-01T00:00:00'));
    });
});