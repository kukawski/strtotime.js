var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('monthfull', function () {
  it('should return correct timestamp', function () {
    assert.strictEqual(strtotime('january', now), ts('2017-01-14T00:00:00'));
    assert.strictEqual(strtotime('february', now), ts('2017-02-14T00:00:00'));
    assert.strictEqual(strtotime('march', now), ts('2017-03-14T00:00:00'));
    assert.strictEqual(strtotime('april', now), ts('2017-04-14T00:00:00'));
    assert.strictEqual(strtotime('may', now), ts('2017-05-14T00:00:00'));
    assert.strictEqual(strtotime('june', now), ts('2017-06-14T00:00:00'));
    assert.strictEqual(strtotime('july', now), ts('2017-07-14T00:00:00'));
    assert.strictEqual(strtotime('august', now), ts('2017-08-14T00:00:00'));
    assert.strictEqual(strtotime('september', now), ts('2017-09-14T00:00:00'));
    assert.strictEqual(strtotime('october', now), ts('2017-10-14T00:00:00'));
    assert.strictEqual(strtotime('november', now), ts('2017-11-14T00:00:00'));
    assert.strictEqual(strtotime('december', now), ts('2017-12-14T00:00:00'));
  });
});

describe('monthabbr', function () {
  it('should return correct timestamp', function () {
    assert.strictEqual(strtotime('jan', now), ts('2017-01-14T00:00:00'));
    assert.strictEqual(strtotime('feb', now), ts('2017-02-14T00:00:00'));
    assert.strictEqual(strtotime('mar', now), ts('2017-03-14T00:00:00'));
    assert.strictEqual(strtotime('apr', now), ts('2017-04-14T00:00:00'));
    assert.strictEqual(strtotime('may', now), ts('2017-05-14T00:00:00'));
    assert.strictEqual(strtotime('jun', now), ts('2017-06-14T00:00:00'));
    assert.strictEqual(strtotime('jul', now), ts('2017-07-14T00:00:00'));
    assert.strictEqual(strtotime('aug', now), ts('2017-08-14T00:00:00'));
    assert.strictEqual(strtotime('sep', now), ts('2017-09-14T00:00:00'));
    assert.strictEqual(strtotime('sept', now), ts('2017-09-14T00:00:00'));
    assert.strictEqual(strtotime('oct', now), ts('2017-10-14T00:00:00'));
    assert.strictEqual(strtotime('nov', now), ts('2017-11-14T00:00:00'));
    assert.strictEqual(strtotime('dec', now), ts('2017-12-14T00:00:00'));
  });
});