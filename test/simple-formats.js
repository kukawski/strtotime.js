var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('yesterday', function () {
    it('should return yesterday midnight', function () {
        assert.strictEqual(strtotime('yesterday', now), ts('2017-03-13T00:00:00'));
    });
});

describe('now', function () {
    it('should return current timestamp', function () {
        assert.strictEqual(strtotime('now', now), now);
    });
});

describe('noon', function () {
    it('should return today noon', function () {
        assert.strictEqual(strtotime('noon', now), ts('2017-03-14T12:00:00'));
    });
});

describe('midnight', function () {
    it('should return today midnight', function () {
        assert.strictEqual(strtotime('midnight', now), ts('2017-03-14T00:00:00'));
    });
});

describe('today', function () {
    it('should return today midnight', function () {
        assert.strictEqual(strtotime('today', now), ts('2017-03-14T00:00:00'));
    });
});

describe('tomorrow', function () {
    it('should return tomorrow midnight', function () {
        assert.strictEqual(strtotime('tomorrow', now), ts('2017-03-15T00:00:00'));
    });
});

describe('timestamp', function () {
    it('should return correct timestamp', function () {
        assert.strictEqual(strtotime('@0'), 0);
        assert.strictEqual(strtotime('@123456789'), 123456789);
        assert.strictEqual(strtotime('@-123456789'), -123456789);
    });
});