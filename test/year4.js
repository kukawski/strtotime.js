var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('year4', function () {
    it('should return correct timestamp', function () {
        assert.strictEqual(strtotime('0060', now), ts('0060-03-14T12:30:45'));
        assert.strictEqual(strtotime('2460', now), ts('2460-03-14T12:30:45'));
        assert.strictEqual(strtotime('2500', now), ts('2500-03-14T12:30:45'));
        assert.strictEqual(strtotime('9999', now), ts('9999-03-14T12:30:45'));
    });

    it('should treat numbers [00-24][00-59] as time (see gnunocolon)', function () {
        assert.strictEqual(strtotime('0000', now), ts('2017-03-14T00:00:00'));
        assert.strictEqual(strtotime('2459', now), ts('2017-03-15T00:59:00'));
    })
});