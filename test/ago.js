var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('ago', function () {
    it('should work together with yesterday', function () {
        assert.strictEqual(strtotime('yesterday ago', now), ts('2017-03-15T00:00:00'));
    });

    it('should work together with tomorrow', function () {
        assert.strictEqual(strtotime('tomorrow ago', now), ts('2017-03-13T00:00:00'));
    });

    it('should work together with timestamps', function () {
        assert.strictEqual(strtotime('@123456 ago', now), -123456);
        assert.strictEqual(strtotime('@-123456 ago', now), 123456);
    });

    it('should work together with isoweek', function () {
        assert.strictEqual(strtotime('2017-W07 ago', now), ts('2016-11-19T00:00:00'));
    });

    it('should work together with relativetext', function () {
        assert.strictEqual(strtotime('first year second month third day fourth hour fifth minute sixth second ago', now), ts('2016-01-11T08:25:39'));
    });

    it('should work together with relative', function () {
        assert.strictEqual(strtotime('+1 year +2 months +3 days +4 hours +5 minutes +6 seconds ago', now), ts('2016-01-11T08:25:39'));
        assert.strictEqual(strtotime('-1 year -2 months -3 days -4 hours -5 minutes -6 seconds ago', now), ts('2018-05-17T16:35:51'));
    });

    it('order of rules makes a difference', function () {
        assert.strictEqual(strtotime('ago yesterday', now), ts('2017-03-13T00:00:00'), 'works as yesterday, because ago reversed zeros');
        assert.strictEqual(strtotime('ago tomorrow', now), ts('2017-03-15T00:00:00'));
    });

    it('multiple ago possible and reverse the result', function () {
        assert.strictEqual(strtotime('yesterday ago ago', now), ts('2017-03-13T00:00:00'));
        assert.strictEqual(strtotime('yesterday ago ago ago', now), ts('2017-03-15T00:00:00'));
    });
});