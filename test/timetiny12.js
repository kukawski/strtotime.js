var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('timetiny12', function () {
    it('should return timestamp for today (relative to now) 1am', function () {
        var expected = ts('2017-03-14T01:00:00');

        assert.strictEqual(strtotime('1am', now), expected);
        assert.strictEqual(strtotime('1a.m', now), expected);
        assert.strictEqual(strtotime('1am.', now), expected);
        assert.strictEqual(strtotime('1a.m.', now), expected);
        assert.strictEqual(strtotime('1 am', now), expected);
        assert.strictEqual(strtotime('1 a.m', now), expected);
        assert.strictEqual(strtotime('1 am.', now), expected);
        assert.strictEqual(strtotime('1 a.m.', now), expected);

        assert.strictEqual(strtotime('01am', now), expected);
        assert.strictEqual(strtotime('01a.m', now), expected);
        assert.strictEqual(strtotime('01am.', now), expected);
        assert.strictEqual(strtotime('01a.m.', now), expected);
        assert.strictEqual(strtotime('01 am', now), expected);
        assert.strictEqual(strtotime('01 a.m', now), expected);
        assert.strictEqual(strtotime('01 am.', now), expected);
        assert.strictEqual(strtotime('01 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 2am', function () {
        var expected = ts('2017-03-14T02:00:00');

        assert.strictEqual(strtotime('2am', now), expected);
        assert.strictEqual(strtotime('2a.m', now), expected);
        assert.strictEqual(strtotime('2am.', now), expected);
        assert.strictEqual(strtotime('2a.m.', now), expected);
        assert.strictEqual(strtotime('2 am', now), expected);
        assert.strictEqual(strtotime('2 a.m', now), expected);
        assert.strictEqual(strtotime('2 am.', now), expected);
        assert.strictEqual(strtotime('2 a.m.', now), expected);

        assert.strictEqual(strtotime('02am', now), expected);
        assert.strictEqual(strtotime('02a.m', now), expected);
        assert.strictEqual(strtotime('02am.', now), expected);
        assert.strictEqual(strtotime('02a.m.', now), expected);
        assert.strictEqual(strtotime('02 am', now), expected);
        assert.strictEqual(strtotime('02 a.m', now), expected);
        assert.strictEqual(strtotime('02 am.', now), expected);
        assert.strictEqual(strtotime('02 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 3am', function () {
        var expected = ts('2017-03-14T03:00:00');

        assert.strictEqual(strtotime('3am', now), expected);
        assert.strictEqual(strtotime('3a.m', now), expected);
        assert.strictEqual(strtotime('3am.', now), expected);
        assert.strictEqual(strtotime('3a.m.', now), expected);
        assert.strictEqual(strtotime('3 am', now), expected);
        assert.strictEqual(strtotime('3 a.m', now), expected);
        assert.strictEqual(strtotime('3 am.', now), expected);
        assert.strictEqual(strtotime('3 a.m.', now), expected);

        assert.strictEqual(strtotime('03am', now), expected);
        assert.strictEqual(strtotime('03a.m', now), expected);
        assert.strictEqual(strtotime('03am.', now), expected);
        assert.strictEqual(strtotime('03a.m.', now), expected);
        assert.strictEqual(strtotime('03 am', now), expected);
        assert.strictEqual(strtotime('03 a.m', now), expected);
        assert.strictEqual(strtotime('03 am.', now), expected);
        assert.strictEqual(strtotime('03 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 4am', function () {
        var expected = ts('2017-03-14T04:00:00');

        assert.strictEqual(strtotime('4am', now), expected);
        assert.strictEqual(strtotime('4a.m', now), expected);
        assert.strictEqual(strtotime('4am.', now), expected);
        assert.strictEqual(strtotime('4a.m.', now), expected);
        assert.strictEqual(strtotime('4 am', now), expected);
        assert.strictEqual(strtotime('4 a.m', now), expected);
        assert.strictEqual(strtotime('4 am.', now), expected);
        assert.strictEqual(strtotime('4 a.m.', now), expected);

        assert.strictEqual(strtotime('04am', now), expected);
        assert.strictEqual(strtotime('04a.m', now), expected);
        assert.strictEqual(strtotime('04am.', now), expected);
        assert.strictEqual(strtotime('04a.m.', now), expected);
        assert.strictEqual(strtotime('04 am', now), expected);
        assert.strictEqual(strtotime('04 a.m', now), expected);
        assert.strictEqual(strtotime('04 am.', now), expected);
        assert.strictEqual(strtotime('04 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 5am', function () {
        var expected = ts('2017-03-14T05:00:00');

        assert.strictEqual(strtotime('5am', now), expected);
        assert.strictEqual(strtotime('5a.m', now), expected);
        assert.strictEqual(strtotime('5am.', now), expected);
        assert.strictEqual(strtotime('5a.m.', now), expected);
        assert.strictEqual(strtotime('5 am', now), expected);
        assert.strictEqual(strtotime('5 a.m', now), expected);
        assert.strictEqual(strtotime('5 am.', now), expected);
        assert.strictEqual(strtotime('5 a.m.', now), expected);

        assert.strictEqual(strtotime('05am', now), expected);
        assert.strictEqual(strtotime('05a.m', now), expected);
        assert.strictEqual(strtotime('05am.', now), expected);
        assert.strictEqual(strtotime('05a.m.', now), expected);
        assert.strictEqual(strtotime('05 am', now), expected);
        assert.strictEqual(strtotime('05 a.m', now), expected);
        assert.strictEqual(strtotime('05 am.', now), expected);
        assert.strictEqual(strtotime('05 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 6am', function () {
        var expected = ts('2017-03-14T06:00:00');

        assert.strictEqual(strtotime('6am', now), expected);
        assert.strictEqual(strtotime('6a.m', now), expected);
        assert.strictEqual(strtotime('6am.', now), expected);
        assert.strictEqual(strtotime('6a.m.', now), expected);
        assert.strictEqual(strtotime('6 am', now), expected);
        assert.strictEqual(strtotime('6 a.m', now), expected);
        assert.strictEqual(strtotime('6 am.', now), expected);
        assert.strictEqual(strtotime('6 a.m.', now), expected);

        assert.strictEqual(strtotime('06am', now), expected);
        assert.strictEqual(strtotime('06a.m', now), expected);
        assert.strictEqual(strtotime('06am.', now), expected);
        assert.strictEqual(strtotime('06a.m.', now), expected);
        assert.strictEqual(strtotime('06 am', now), expected);
        assert.strictEqual(strtotime('06 a.m', now), expected);
        assert.strictEqual(strtotime('06 am.', now), expected);
        assert.strictEqual(strtotime('06 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 7am', function () {
        var expected = ts('2017-03-14T07:00:00');

        assert.strictEqual(strtotime('7am', now), expected);
        assert.strictEqual(strtotime('7a.m', now), expected);
        assert.strictEqual(strtotime('7am.', now), expected);
        assert.strictEqual(strtotime('7a.m.', now), expected);
        assert.strictEqual(strtotime('7 am', now), expected);
        assert.strictEqual(strtotime('7 a.m', now), expected);
        assert.strictEqual(strtotime('7 am.', now), expected);
        assert.strictEqual(strtotime('7 a.m.', now), expected);

        assert.strictEqual(strtotime('07am', now), expected);
        assert.strictEqual(strtotime('07a.m', now), expected);
        assert.strictEqual(strtotime('07am.', now), expected);
        assert.strictEqual(strtotime('07a.m.', now), expected);
        assert.strictEqual(strtotime('07 am', now), expected);
        assert.strictEqual(strtotime('07 a.m', now), expected);
        assert.strictEqual(strtotime('07 am.', now), expected);
        assert.strictEqual(strtotime('07 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 8am', function () {
        var expected = ts('2017-03-14T08:00:00');

        assert.strictEqual(strtotime('8am', now), expected);
        assert.strictEqual(strtotime('8a.m', now), expected);
        assert.strictEqual(strtotime('8am.', now), expected);
        assert.strictEqual(strtotime('8a.m.', now), expected);
        assert.strictEqual(strtotime('8 am', now), expected);
        assert.strictEqual(strtotime('8 a.m', now), expected);
        assert.strictEqual(strtotime('8 am.', now), expected);
        assert.strictEqual(strtotime('8 a.m.', now), expected);

        assert.strictEqual(strtotime('08am', now), expected);
        assert.strictEqual(strtotime('08a.m', now), expected);
        assert.strictEqual(strtotime('08am.', now), expected);
        assert.strictEqual(strtotime('08a.m.', now), expected);
        assert.strictEqual(strtotime('08 am', now), expected);
        assert.strictEqual(strtotime('08 a.m', now), expected);
        assert.strictEqual(strtotime('08 am.', now), expected);
        assert.strictEqual(strtotime('08 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 9am', function () {
        var expected = ts('2017-03-14T09:00:00');

        assert.strictEqual(strtotime('9am', now), expected);
        assert.strictEqual(strtotime('9a.m', now), expected);
        assert.strictEqual(strtotime('9am.', now), expected);
        assert.strictEqual(strtotime('9a.m.', now), expected);
        assert.strictEqual(strtotime('9 am', now), expected);
        assert.strictEqual(strtotime('9 a.m', now), expected);
        assert.strictEqual(strtotime('9 am.', now), expected);
        assert.strictEqual(strtotime('9 a.m.', now), expected);

        assert.strictEqual(strtotime('09am', now), expected);
        assert.strictEqual(strtotime('09a.m', now), expected);
        assert.strictEqual(strtotime('09am.', now), expected);
        assert.strictEqual(strtotime('09a.m.', now), expected);
        assert.strictEqual(strtotime('09 am', now), expected);
        assert.strictEqual(strtotime('09 a.m', now), expected);
        assert.strictEqual(strtotime('09 am.', now), expected);
        assert.strictEqual(strtotime('09 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 10am', function () {
        var expected = ts('2017-03-14T10:00:00');

        assert.strictEqual(strtotime('10am', now), expected);
        assert.strictEqual(strtotime('10a.m', now), expected);
        assert.strictEqual(strtotime('10am.', now), expected);
        assert.strictEqual(strtotime('10a.m.', now), expected);
        assert.strictEqual(strtotime('10 am', now), expected);
        assert.strictEqual(strtotime('10 a.m', now), expected);
        assert.strictEqual(strtotime('10 am.', now), expected);
        assert.strictEqual(strtotime('10 a.m.', now), expected);

        assert.strictEqual(strtotime('10am', now), expected);
        assert.strictEqual(strtotime('10a.m', now), expected);
        assert.strictEqual(strtotime('10am.', now), expected);
        assert.strictEqual(strtotime('10a.m.', now), expected);
        assert.strictEqual(strtotime('10 am', now), expected);
        assert.strictEqual(strtotime('10 a.m', now), expected);
        assert.strictEqual(strtotime('10 am.', now), expected);
        assert.strictEqual(strtotime('10 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 11am', function () {
        var expected = ts('2017-03-14T11:00:00');

        assert.strictEqual(strtotime('11am', now), expected);
        assert.strictEqual(strtotime('11a.m', now), expected);
        assert.strictEqual(strtotime('11am.', now), expected);
        assert.strictEqual(strtotime('11a.m.', now), expected);
        assert.strictEqual(strtotime('11 am', now), expected);
        assert.strictEqual(strtotime('11 a.m', now), expected);
        assert.strictEqual(strtotime('11 am.', now), expected);
        assert.strictEqual(strtotime('11 a.m.', now), expected);

        assert.strictEqual(strtotime('11am', now), expected);
        assert.strictEqual(strtotime('11a.m', now), expected);
        assert.strictEqual(strtotime('11am.', now), expected);
        assert.strictEqual(strtotime('11a.m.', now), expected);
        assert.strictEqual(strtotime('11 am', now), expected);
        assert.strictEqual(strtotime('11 a.m', now), expected);
        assert.strictEqual(strtotime('11 am.', now), expected);
        assert.strictEqual(strtotime('11 a.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 12am', function () {
        var expected = ts('2017-03-14T00:00:00');

        assert.strictEqual(strtotime('12am', now), expected);
        assert.strictEqual(strtotime('12a.m', now), expected);
        assert.strictEqual(strtotime('12am.', now), expected);
        assert.strictEqual(strtotime('12a.m.', now), expected);
        assert.strictEqual(strtotime('12 am', now), expected);
        assert.strictEqual(strtotime('12 a.m', now), expected);
        assert.strictEqual(strtotime('12 am.', now), expected);
        assert.strictEqual(strtotime('12 a.m.', now), expected);

        assert.strictEqual(strtotime('12am', now), expected);
        assert.strictEqual(strtotime('12a.m', now), expected);
        assert.strictEqual(strtotime('12am.', now), expected);
        assert.strictEqual(strtotime('12a.m.', now), expected);
        assert.strictEqual(strtotime('12 am', now), expected);
        assert.strictEqual(strtotime('12 a.m', now), expected);
        assert.strictEqual(strtotime('12 am.', now), expected);
        assert.strictEqual(strtotime('12 a.m.', now), expected);
    });

    // ---------
    it('should return timestamp for today (relative to now) 1pm', function () {
        var expected = ts('2017-03-14T13:00:00');

        assert.strictEqual(strtotime('1pm', now), expected);
        assert.strictEqual(strtotime('1p.m', now), expected);
        assert.strictEqual(strtotime('1pm.', now), expected);
        assert.strictEqual(strtotime('1p.m.', now), expected);
        assert.strictEqual(strtotime('1 pm', now), expected);
        assert.strictEqual(strtotime('1 p.m', now), expected);
        assert.strictEqual(strtotime('1 pm.', now), expected);
        assert.strictEqual(strtotime('1 p.m.', now), expected);

        assert.strictEqual(strtotime('01pm', now), expected);
        assert.strictEqual(strtotime('01p.m', now), expected);
        assert.strictEqual(strtotime('01pm.', now), expected);
        assert.strictEqual(strtotime('01p.m.', now), expected);
        assert.strictEqual(strtotime('01 pm', now), expected);
        assert.strictEqual(strtotime('01 p.m', now), expected);
        assert.strictEqual(strtotime('01 pm.', now), expected);
        assert.strictEqual(strtotime('01 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 2pm', function () {
        var expected = ts('2017-03-14T14:00:00');

        assert.strictEqual(strtotime('2pm', now), expected);
        assert.strictEqual(strtotime('2p.m', now), expected);
        assert.strictEqual(strtotime('2pm.', now), expected);
        assert.strictEqual(strtotime('2p.m.', now), expected);
        assert.strictEqual(strtotime('2 pm', now), expected);
        assert.strictEqual(strtotime('2 p.m', now), expected);
        assert.strictEqual(strtotime('2 pm.', now), expected);
        assert.strictEqual(strtotime('2 p.m.', now), expected);

        assert.strictEqual(strtotime('02pm', now), expected);
        assert.strictEqual(strtotime('02p.m', now), expected);
        assert.strictEqual(strtotime('02pm.', now), expected);
        assert.strictEqual(strtotime('02p.m.', now), expected);
        assert.strictEqual(strtotime('02 pm', now), expected);
        assert.strictEqual(strtotime('02 p.m', now), expected);
        assert.strictEqual(strtotime('02 pm.', now), expected);
        assert.strictEqual(strtotime('02 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 3pm', function () {
        var expected = ts('2017-03-14T15:00:00');

        assert.strictEqual(strtotime('3pm', now), expected);
        assert.strictEqual(strtotime('3p.m', now), expected);
        assert.strictEqual(strtotime('3pm.', now), expected);
        assert.strictEqual(strtotime('3p.m.', now), expected);
        assert.strictEqual(strtotime('3 pm', now), expected);
        assert.strictEqual(strtotime('3 p.m', now), expected);
        assert.strictEqual(strtotime('3 pm.', now), expected);
        assert.strictEqual(strtotime('3 p.m.', now), expected);

        assert.strictEqual(strtotime('03pm', now), expected);
        assert.strictEqual(strtotime('03p.m', now), expected);
        assert.strictEqual(strtotime('03pm.', now), expected);
        assert.strictEqual(strtotime('03p.m.', now), expected);
        assert.strictEqual(strtotime('03 pm', now), expected);
        assert.strictEqual(strtotime('03 p.m', now), expected);
        assert.strictEqual(strtotime('03 pm.', now), expected);
        assert.strictEqual(strtotime('03 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 4pm', function () {
        var expected = ts('2017-03-14T16:00:00');

        assert.strictEqual(strtotime('4pm', now), expected);
        assert.strictEqual(strtotime('4p.m', now), expected);
        assert.strictEqual(strtotime('4pm.', now), expected);
        assert.strictEqual(strtotime('4p.m.', now), expected);
        assert.strictEqual(strtotime('4 pm', now), expected);
        assert.strictEqual(strtotime('4 p.m', now), expected);
        assert.strictEqual(strtotime('4 pm.', now), expected);
        assert.strictEqual(strtotime('4 p.m.', now), expected);

        assert.strictEqual(strtotime('04pm', now), expected);
        assert.strictEqual(strtotime('04p.m', now), expected);
        assert.strictEqual(strtotime('04pm.', now), expected);
        assert.strictEqual(strtotime('04p.m.', now), expected);
        assert.strictEqual(strtotime('04 pm', now), expected);
        assert.strictEqual(strtotime('04 p.m', now), expected);
        assert.strictEqual(strtotime('04 pm.', now), expected);
        assert.strictEqual(strtotime('04 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 5pm', function () {
        var expected = ts('2017-03-14T17:00:00');

        assert.strictEqual(strtotime('5pm', now), expected);
        assert.strictEqual(strtotime('5p.m', now), expected);
        assert.strictEqual(strtotime('5pm.', now), expected);
        assert.strictEqual(strtotime('5p.m.', now), expected);
        assert.strictEqual(strtotime('5 pm', now), expected);
        assert.strictEqual(strtotime('5 p.m', now), expected);
        assert.strictEqual(strtotime('5 pm.', now), expected);
        assert.strictEqual(strtotime('5 p.m.', now), expected);

        assert.strictEqual(strtotime('05pm', now), expected);
        assert.strictEqual(strtotime('05p.m', now), expected);
        assert.strictEqual(strtotime('05pm.', now), expected);
        assert.strictEqual(strtotime('05p.m.', now), expected);
        assert.strictEqual(strtotime('05 pm', now), expected);
        assert.strictEqual(strtotime('05 p.m', now), expected);
        assert.strictEqual(strtotime('05 pm.', now), expected);
        assert.strictEqual(strtotime('05 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 6pm', function () {
        var expected = ts('2017-03-14T18:00:00');

        assert.strictEqual(strtotime('6pm', now), expected);
        assert.strictEqual(strtotime('6p.m', now), expected);
        assert.strictEqual(strtotime('6pm.', now), expected);
        assert.strictEqual(strtotime('6p.m.', now), expected);
        assert.strictEqual(strtotime('6 pm', now), expected);
        assert.strictEqual(strtotime('6 p.m', now), expected);
        assert.strictEqual(strtotime('6 pm.', now), expected);
        assert.strictEqual(strtotime('6 p.m.', now), expected);

        assert.strictEqual(strtotime('06pm', now), expected);
        assert.strictEqual(strtotime('06p.m', now), expected);
        assert.strictEqual(strtotime('06pm.', now), expected);
        assert.strictEqual(strtotime('06p.m.', now), expected);
        assert.strictEqual(strtotime('06 pm', now), expected);
        assert.strictEqual(strtotime('06 p.m', now), expected);
        assert.strictEqual(strtotime('06 pm.', now), expected);
        assert.strictEqual(strtotime('06 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 7pm', function () {
        var expected = ts('2017-03-14T19:00:00');

        assert.strictEqual(strtotime('7pm', now), expected);
        assert.strictEqual(strtotime('7p.m', now), expected);
        assert.strictEqual(strtotime('7pm.', now), expected);
        assert.strictEqual(strtotime('7p.m.', now), expected);
        assert.strictEqual(strtotime('7 pm', now), expected);
        assert.strictEqual(strtotime('7 p.m', now), expected);
        assert.strictEqual(strtotime('7 pm.', now), expected);
        assert.strictEqual(strtotime('7 p.m.', now), expected);

        assert.strictEqual(strtotime('07pm', now), expected);
        assert.strictEqual(strtotime('07p.m', now), expected);
        assert.strictEqual(strtotime('07pm.', now), expected);
        assert.strictEqual(strtotime('07p.m.', now), expected);
        assert.strictEqual(strtotime('07 pm', now), expected);
        assert.strictEqual(strtotime('07 p.m', now), expected);
        assert.strictEqual(strtotime('07 pm.', now), expected);
        assert.strictEqual(strtotime('07 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 8pm', function () {
        var expected = ts('2017-03-14T20:00:00');

        assert.strictEqual(strtotime('8pm', now), expected);
        assert.strictEqual(strtotime('8p.m', now), expected);
        assert.strictEqual(strtotime('8pm.', now), expected);
        assert.strictEqual(strtotime('8p.m.', now), expected);
        assert.strictEqual(strtotime('8 pm', now), expected);
        assert.strictEqual(strtotime('8 p.m', now), expected);
        assert.strictEqual(strtotime('8 pm.', now), expected);
        assert.strictEqual(strtotime('8 p.m.', now), expected);

        assert.strictEqual(strtotime('08pm', now), expected);
        assert.strictEqual(strtotime('08p.m', now), expected);
        assert.strictEqual(strtotime('08pm.', now), expected);
        assert.strictEqual(strtotime('08p.m.', now), expected);
        assert.strictEqual(strtotime('08 pm', now), expected);
        assert.strictEqual(strtotime('08 p.m', now), expected);
        assert.strictEqual(strtotime('08 pm.', now), expected);
        assert.strictEqual(strtotime('08 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 9pm', function () {
        var expected = ts('2017-03-14T21:00:00');

        assert.strictEqual(strtotime('9pm', now), expected);
        assert.strictEqual(strtotime('9p.m', now), expected);
        assert.strictEqual(strtotime('9pm.', now), expected);
        assert.strictEqual(strtotime('9p.m.', now), expected);
        assert.strictEqual(strtotime('9 pm', now), expected);
        assert.strictEqual(strtotime('9 p.m', now), expected);
        assert.strictEqual(strtotime('9 pm.', now), expected);
        assert.strictEqual(strtotime('9 p.m.', now), expected);

        assert.strictEqual(strtotime('09pm', now), expected);
        assert.strictEqual(strtotime('09p.m', now), expected);
        assert.strictEqual(strtotime('09pm.', now), expected);
        assert.strictEqual(strtotime('09p.m.', now), expected);
        assert.strictEqual(strtotime('09 pm', now), expected);
        assert.strictEqual(strtotime('09 p.m', now), expected);
        assert.strictEqual(strtotime('09 pm.', now), expected);
        assert.strictEqual(strtotime('09 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 10pm', function () {
        var expected = ts('2017-03-14T22:00:00');

        assert.strictEqual(strtotime('10pm', now), expected);
        assert.strictEqual(strtotime('10p.m', now), expected);
        assert.strictEqual(strtotime('10pm.', now), expected);
        assert.strictEqual(strtotime('10p.m.', now), expected);
        assert.strictEqual(strtotime('10 pm', now), expected);
        assert.strictEqual(strtotime('10 p.m', now), expected);
        assert.strictEqual(strtotime('10 pm.', now), expected);
        assert.strictEqual(strtotime('10 p.m.', now), expected);

        assert.strictEqual(strtotime('10pm', now), expected);
        assert.strictEqual(strtotime('10p.m', now), expected);
        assert.strictEqual(strtotime('10pm.', now), expected);
        assert.strictEqual(strtotime('10p.m.', now), expected);
        assert.strictEqual(strtotime('10 pm', now), expected);
        assert.strictEqual(strtotime('10 p.m', now), expected);
        assert.strictEqual(strtotime('10 pm.', now), expected);
        assert.strictEqual(strtotime('10 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 11pm', function () {
        var expected = ts('2017-03-14T23:00:00');

        assert.strictEqual(strtotime('11pm', now), expected);
        assert.strictEqual(strtotime('11p.m', now), expected);
        assert.strictEqual(strtotime('11pm.', now), expected);
        assert.strictEqual(strtotime('11p.m.', now), expected);
        assert.strictEqual(strtotime('11 pm', now), expected);
        assert.strictEqual(strtotime('11 p.m', now), expected);
        assert.strictEqual(strtotime('11 pm.', now), expected);
        assert.strictEqual(strtotime('11 p.m.', now), expected);

        assert.strictEqual(strtotime('11pm', now), expected);
        assert.strictEqual(strtotime('11p.m', now), expected);
        assert.strictEqual(strtotime('11pm.', now), expected);
        assert.strictEqual(strtotime('11p.m.', now), expected);
        assert.strictEqual(strtotime('11 pm', now), expected);
        assert.strictEqual(strtotime('11 p.m', now), expected);
        assert.strictEqual(strtotime('11 pm.', now), expected);
        assert.strictEqual(strtotime('11 p.m.', now), expected);
    });

    it('should return timestamp for today (relative to now) 12pm', function () {
        var expected = ts('2017-03-14T12:00:00');

        assert.strictEqual(strtotime('12pm', now), expected);
        assert.strictEqual(strtotime('12p.m', now), expected);
        assert.strictEqual(strtotime('12pm.', now), expected);
        assert.strictEqual(strtotime('12p.m.', now), expected);
        assert.strictEqual(strtotime('12 pm', now), expected);
        assert.strictEqual(strtotime('12 p.m', now), expected);
        assert.strictEqual(strtotime('12 pm.', now), expected);
        assert.strictEqual(strtotime('12 p.m.', now), expected);

        assert.strictEqual(strtotime('12pm', now), expected);
        assert.strictEqual(strtotime('12p.m', now), expected);
        assert.strictEqual(strtotime('12pm.', now), expected);
        assert.strictEqual(strtotime('12p.m.', now), expected);
        assert.strictEqual(strtotime('12 pm', now), expected);
        assert.strictEqual(strtotime('12 p.m', now), expected);
        assert.strictEqual(strtotime('12 pm.', now), expected);
        assert.strictEqual(strtotime('12 p.m.', now), expected);
    });
});