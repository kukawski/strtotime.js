var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('first day of', function () {
    it('should return first day of month relative to now', function () {
        assert.strictEqual(strtotime('first day of', now), ts('2017-03-01T12:30:45'));
    });

    it('first day of is relative to month', function () {
        assert.strictEqual(strtotime('first day of may', now), ts('2017-05-01T00:00:00'));
        assert.strictEqual(strtotime('first day of 2500', now), ts('2500-03-01T12:30:45'));
    });
});

describe('last day of', function () {
    it('should return last day of month relative to now', function () {
        assert.strictEqual(strtotime('last day of', now), ts('2017-03-31T12:30:45'));
    });

    it('last day of is relative to month', function () {
        assert.strictEqual(strtotime('last day of may', now), ts('2017-05-31T00:00:00'));
        assert.strictEqual(strtotime('last day of 2500', now), ts('2500-03-31T12:30:45'));
        assert.strictEqual(strtotime('last day of february', ts('2017-03-31T12:30:45')), ts('2017-02-28T00:00:00'));
    });
});
