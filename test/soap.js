var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('soap', function () {
    it('should return correct timestamp', function () {
        assert.strictEqual(strtotime('2017-09-13T00:00:00.000', now), ts('2017-09-13T00:00:00'));
    });

    it('should handle 0th month and 0th day', function () {
        assert.strictEqual(strtotime('2017-00-13T00:00:00.000', now), ts('2016-12-13T00:00:00'));
        assert.strictEqual(strtotime('2017-09-00T00:00:00.000', now), ts('2017-08-31T00:00:00'));
        assert.strictEqual(strtotime('2017-00-00T00:00:00.000', now), ts('2016-11-30T00:00:00'));
    });

    it('should handle day out of bounds', function () {
        assert.strictEqual(strtotime('2017-02-29T00:00:00.000', now), ts('2017-03-01T00:00:00'));
        assert.strictEqual(strtotime('2016-02-30T00:00:00.000', now), ts('2016-03-01T00:00:00'));
        assert.strictEqual(strtotime('2017-04-31T00:00:00.000', now), ts('2017-05-01T00:00:00'));
    });

    it('should handle 24th hour and 60th sec', function () {
        assert.strictEqual(strtotime('2017-09-13T24:00:00.000', now), ts('2017-09-14T00:00:00'));
        assert.strictEqual(strtotime('2017-09-13T00:00:60.000', now), ts('2017-09-13T00:01:00'));
        assert.strictEqual(strtotime('2017-09-13T24:00:60.000', now), ts('2017-09-14T00:01:00'));
        assert.strictEqual(strtotime('2017-12-31T24:59:60.000', now), ts('2018-01-01T01:00:00'));
    });

    it('should handle tz correction', function () {
        assert.strictEqual(strtotime('2017-09-13T00:00:00.000GMT-02:00', now), ts('2017-09-13T02:00:00Z'));
        assert.strictEqual(strtotime('2017-00-13T00:00:00.000GMT-02:00', now), ts('2016-12-13T02:00:00Z'));
        assert.strictEqual(strtotime('2017-09-00T00:00:00.000GMT-02:00', now), ts('2017-08-31T02:00:00Z'));
        assert.strictEqual(strtotime('2017-00-00T00:00:00.000GMT-02:00', now), ts('2016-11-30T02:00:00Z'));
        assert.strictEqual(strtotime('2017-02-29T00:00:00.000GMT-02:00', now), ts('2017-03-01T02:00:00Z'));
        assert.strictEqual(strtotime('2016-02-30T00:00:00.000GMT-02:00', now), ts('2016-03-01T02:00:00Z'));
        assert.strictEqual(strtotime('2017-04-31T00:00:00.000GMT-02:00', now), ts('2017-05-01T02:00:00Z'));
        assert.strictEqual(strtotime('2017-09-13T24:00:00.000GMT-02:00', now), ts('2017-09-14T02:00:00Z'));
        assert.strictEqual(strtotime('2017-09-13T00:00:60.000GMT-02:00', now), ts('2017-09-13T02:01:00Z'));
        assert.strictEqual(strtotime('2017-09-13T24:00:60.000GMT-02:00', now), ts('2017-09-14T02:01:00Z'));
        assert.strictEqual(strtotime('2017-12-31T24:59:60.000GMT-02:00', now), ts('2018-01-01T03:00:00Z'));
    });
});