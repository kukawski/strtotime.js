var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('timelong12', function () {
    it('should return correct timestamp', function () {
        assert.strictEqual(strtotime('1:1:01am ', now), ts('2017-03-14T01:01:01'));
        assert.strictEqual(strtotime('2:02:02am ', now), ts('2017-03-14T02:02:02'));
        assert.strictEqual(strtotime('3:3:03a.m ', now), ts('2017-03-14T03:03:03'));
        assert.strictEqual(strtotime('4:4:04am. ', now), ts('2017-03-14T04:04:04'));
        assert.strictEqual(strtotime('5:05:05a.m. ', now), ts('2017-03-14T05:05:05'));
        assert.strictEqual(strtotime('6:6:06 am ', now), ts('2017-03-14T06:06:06'));
        assert.strictEqual(strtotime('7:7:07 a.m ', now), ts('2017-03-14T07:07:07'));
        assert.strictEqual(strtotime('8:8:08 am. ', now), ts('2017-03-14T08:08:08'));
        assert.strictEqual(strtotime('9:9:09 a.m. ', now), ts('2017-03-14T09:09:09'));
    });
});