var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('datenoday', function () {
  it('no separator', function () {
    assert.strictEqual(strtotime('jan2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('feb2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('mar2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('apr2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('jun2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('jul2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('aug2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('sep2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('sept2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('oct2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('nov2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('dec2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('january2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('february2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('march2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('april2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('june2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('july2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('august2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('september2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('october2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('november2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('december2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('I2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('II2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('III2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('IV2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('V2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('VI2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('VII2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('VIII2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('IX2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('X2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('XI2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('XII2007', now), ts('2007-12-01T00:00:00'));
  });

  it('space as separator', function () {
    assert.strictEqual(strtotime('jan 2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('feb 2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('mar 2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('apr 2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may 2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('jun 2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('jul 2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('aug 2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('sep 2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('sept 2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('oct 2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('nov 2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('dec 2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('january 2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('february 2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('march 2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('april 2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may 2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('june 2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('july 2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('august 2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('september 2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('october 2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('november 2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('december 2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('I 2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('II 2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('III 2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('IV 2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('V 2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('VI 2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('VII 2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('VIII 2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('IX 2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('X 2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('XI 2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('XII 2007', now), ts('2007-12-01T00:00:00'));
  });

  it('dot as separator', function () {
    assert.strictEqual(strtotime('jan.2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('feb.2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('mar.2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('apr.2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may.2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('jun.2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('jul.2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('aug.2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('sep.2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('sept.2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('oct.2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('nov.2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('dec.2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('january.2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('february.2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('march.2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('april.2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may.2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('june.2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('july.2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('august.2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('september.2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('october.2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('november.2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('december.2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('I.2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('II.2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('III.2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('IV.2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('V.2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('VI.2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('VII.2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('VIII.2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('IX.2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('X.2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('XI.2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('XII.2007', now), ts('2007-12-01T00:00:00'));
  });

  it('hyphen as separator', function () {
    assert.strictEqual(strtotime('jan-2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('feb-2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('mar-2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('apr-2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may-2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('jun-2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('jul-2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('aug-2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('sep-2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('sept-2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('oct-2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('nov-2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('dec-2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('january-2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('february-2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('march-2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('april-2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may-2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('june-2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('july-2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('august-2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('september-2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('october-2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('november-2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('december-2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('I-2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('II-2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('III-2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('IV-2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('V-2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('VI-2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('VII-2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('VIII-2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('IX-2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('X-2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('XI-2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('XII-2007', now), ts('2007-12-01T00:00:00'));
  });

  it('tab as separator', function () {
    assert.strictEqual(strtotime('jan\t2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('feb\t2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('mar\t2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('apr\t2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may\t2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('jun\t2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('jul\t2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('aug\t2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('sep\t2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('sept\t2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('oct\t2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('nov\t2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('dec\t2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('january\t2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('february\t2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('march\t2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('april\t2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('may\t2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('june\t2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('july\t2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('august\t2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('september\t2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('october\t2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('november\t2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('december\t2007', now), ts('2007-12-01T00:00:00'));
    assert.strictEqual(strtotime('I\t2007', now), ts('2007-01-01T00:00:00'));
    assert.strictEqual(strtotime('II\t2007', now), ts('2007-02-01T00:00:00'));
    assert.strictEqual(strtotime('III\t2007', now), ts('2007-03-01T00:00:00'));
    assert.strictEqual(strtotime('IV\t2007', now), ts('2007-04-01T00:00:00'));
    assert.strictEqual(strtotime('V\t2007', now), ts('2007-05-01T00:00:00'));
    assert.strictEqual(strtotime('VI\t2007', now), ts('2007-06-01T00:00:00'));
    assert.strictEqual(strtotime('VII\t2007', now), ts('2007-07-01T00:00:00'));
    assert.strictEqual(strtotime('VIII\t2007', now), ts('2007-08-01T00:00:00'));
    assert.strictEqual(strtotime('IX\t2007', now), ts('2007-09-01T00:00:00'));
    assert.strictEqual(strtotime('X\t2007', now), ts('2007-10-01T00:00:00'));
    assert.strictEqual(strtotime('XI\t2007', now), ts('2007-11-01T00:00:00'));
    assert.strictEqual(strtotime('XII\t2007', now), ts('2007-12-01T00:00:00'));
  });
});