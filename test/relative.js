var assert = require('assert');
var strtotime = require('../strtotime');
var ts = require('../test-helpers').ts;
var piDay = ts('2017-03-14T12:30:45');
var now = piDay;

describe('relative', function() {
    it('adding/subtracting seconds', function() {
        assert.strictEqual(strtotime('+1 sec', now), ts('2017-03-14T12:30:46'));
        assert.strictEqual(strtotime('+2 secs', now), ts('2017-03-14T12:30:47'));
        assert.strictEqual(strtotime('+3 second', now), ts('2017-03-14T12:30:48'));
        assert.strictEqual(strtotime('+4 seconds', now), ts('2017-03-14T12:30:49'));

        assert.strictEqual(strtotime('-5 sec', now), ts('2017-03-14T12:30:40'));
        assert.strictEqual(strtotime('-6 secs', now), ts('2017-03-14T12:30:39'));
        assert.strictEqual(strtotime('-7 second', now), ts('2017-03-14T12:30:38'));
        assert.strictEqual(strtotime('-8 seconds', now), ts('2017-03-14T12:30:37'));
    });

    it('adding/subtracting minutes', function() {
        assert.strictEqual(strtotime('+1 min', now), ts('2017-03-14T12:31:45'));
        assert.strictEqual(strtotime('+2 mins', now), ts('2017-03-14T12:32:45'));
        assert.strictEqual(strtotime('+3 minute', now), ts('2017-03-14T12:33:45'));
        assert.strictEqual(strtotime('+4 minutes', now), ts('2017-03-14T12:34:45'));

        assert.strictEqual(strtotime('-5 min', now), ts('2017-03-14T12:25:45'));
        assert.strictEqual(strtotime('-6 mins', now), ts('2017-03-14T12:24:45'));
        assert.strictEqual(strtotime('-7 minute', now), ts('2017-03-14T12:23:45'));
        assert.strictEqual(strtotime('-8 minutes', now), ts('2017-03-14T12:22:45'));
    });

    it('adding/subtracting hours', function() {
        assert.strictEqual(strtotime('+1 hour', now), ts('2017-03-14T13:30:45'));
        assert.strictEqual(strtotime('+2 hours', now), ts('2017-03-14T14:30:45'));

        assert.strictEqual(strtotime('-3 hour', now), ts('2017-03-14T09:30:45'));
        assert.strictEqual(strtotime('-4 hours', now), ts('2017-03-14T08:30:45'));
    });

    it('adding/subtracting days', function() {
        assert.strictEqual(strtotime('+1 day', now), ts('2017-03-15T12:30:45'));
        assert.strictEqual(strtotime('+2 days', now), ts('2017-03-16T12:30:45'));

        assert.strictEqual(strtotime('-3 day', now), ts('2017-03-11T12:30:45'));
        assert.strictEqual(strtotime('-4 days', now), ts('2017-03-10T12:30:45'));
    });

    it('adding/subtracting fortnights', function() {
        assert.strictEqual(strtotime('+1 fortnight', now), ts('2017-03-28T12:30:45'));
        assert.strictEqual(strtotime('+2 fortnights', now), ts('2017-04-11T12:30:45'));
        assert.strictEqual(strtotime('+3 forthnight', now), ts('2017-04-25T12:30:45'));
        assert.strictEqual(strtotime('+4 forthnights', now), ts('2017-05-09T12:30:45'));

        assert.strictEqual(strtotime('-5 fortnight', now), ts('2017-01-03T12:30:45'));
        assert.strictEqual(strtotime('-6 fortnights', now), ts('2016-12-20T12:30:45'));
        assert.strictEqual(strtotime('-7 forthnight', now), ts('2016-12-06T12:30:45'));
        assert.strictEqual(strtotime('-8 forthnights', now), ts('2016-11-22T12:30:45'));
    });

    it('adding/subtracting weeks', function() {
        assert.strictEqual(strtotime('+1 week', now), ts('2017-03-21T12:30:45'));
        assert.strictEqual(strtotime('+2 weeks', now), ts('2017-03-28T12:30:45'));

        assert.strictEqual(strtotime('-3 week', now), ts('2017-02-21T12:30:45'));
        assert.strictEqual(strtotime('-4 week', now), ts('2017-02-14T12:30:45'));
    });

    it('adding/subtracting months', function() {
        assert.strictEqual(strtotime('+1 month', now), ts('2017-04-14T12:30:45'));
        assert.strictEqual(strtotime('+2 months', now), ts('2017-05-14T12:30:45'));

        assert.strictEqual(strtotime('-3 month', now), ts('2016-12-14T12:30:45'));
        assert.strictEqual(strtotime('-4 months', now), ts('2016-11-14T12:30:45'));
    });

    it('adding/subtracting years', function() {
        assert.strictEqual(strtotime('+1 year', now), ts('2018-03-14T12:30:45'));
        assert.strictEqual(strtotime('+2 years', now), ts('2019-03-14T12:30:45'));

        assert.strictEqual(strtotime('-3 year', now), ts('2014-03-14T12:30:45'));
        assert.strictEqual(strtotime('-4 years', now), ts('2013-03-14T12:30:45'));
    });

    it('no sign', function() {
        assert.strictEqual(strtotime('1 sec', now), ts('2017-03-14T12:30:46'));
        assert.strictEqual(strtotime('2 min', now), ts('2017-03-14T12:32:45'));
        assert.strictEqual(strtotime('3 hour', now), ts('2017-03-14T15:30:45'));
        assert.strictEqual(strtotime('4 day', now), ts('2017-03-18T12:30:45'));
        assert.strictEqual(strtotime('5 fortnight', now), ts('2017-05-23T12:30:45'));
        assert.strictEqual(strtotime('6 week', now), ts('2017-04-25T12:30:45'));
        assert.strictEqual(strtotime('7 month', now), ts('2017-10-14T12:30:45'));
        assert.strictEqual(strtotime('8 year', now), ts('2025-03-14T12:30:45'));
    });

    it('multiple signs', function() {
        assert.strictEqual(strtotime('--1 day', now), ts('2017-03-15T12:30:45'));
        assert.strictEqual(strtotime('---1 day', now), ts('2017-03-13T12:30:45'));
        assert.strictEqual(strtotime('+-1 day', now), ts('2017-03-13T12:30:45'));
        assert.strictEqual(strtotime('-+1 day', now), ts('2017-03-13T12:30:45'));
        assert.strictEqual(strtotime('+-+1 day', now), ts('2017-03-13T12:30:45'));
        assert.strictEqual(strtotime('-+-1 day', now), ts('2017-03-15T12:30:45'));
    });
});